# Images

Image URLs come from images uploaded into sendgrid so these should always be created from the Our Honest Foods sendgrid account (account ID `sg7eb3455d155e5a63a11a515c22fd5a4a`). Ideally email templates should be mocked up from that account, so we get the correct URLs for our images (rather than a URL linked to some other sendgrid account).
