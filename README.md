# our-honest-foods-templates

Repository for managing notification templates with git and `@rocketmakers/orbit-template-http-repository`

## [Images](./docs/images.md)

## [Layouts](./docs/layouts.md)

## [Partials](./docs/partials.md)

## [`<provider>.json`](./docs/providerJson.md)

## [Managing templates](./docs/managingTemplates.md)
